import java.util.Scanner;
/**
 * @class Main class
 * Description Main class with properties n, a, number, num
 * @author Хорошилова Екатерина
 * @version 1.12
 * @since 29.12
 */
public class rjl {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите количесво чисел:");
        /**
         *Введите количество чисел
         */
        int n = in.nextInt();
        /**
         * Массив элементов
         */
        int[] a = new int[n];
        System.out.println("Введите " + n + " целых чисел, так чтобы был единственный элемент, значение которого принадлежит интервалу от 2 до 5:");
        int number;
        /**
         * Filed number
         */
        inEltmtnt(a);

        number = 0;

        int num;
        /**
         * Filed Item number
         */
        for (num = 0; num < a.length; ++num) {
            if (a[num] >= 2 & a[num] <= 5) {
                number = num;
                System.out.println("элемент и номер элемента: " + a[num] + " " + num);
                /**
                 * Output of the element and the element number
                 */
            }
        }

        for (num = 0; num < number; --num) {
            for (int j = 0; j < number; ++j) {
                if (a[j] < a[j + 1]) {
                    int change = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = change;
                }
            }
        }

        System.out.print("упорядоченный массив: ");
        /**
         * Output of an ordered array
         */

        for (num = 0; num < number; ++num) {
            System.out.print(a[num] + " ");
        }
    }

    /**
     * @method inEltmtnt(int[] a)
     * Description Method Ввод элементов в массив 
     * @param a массив элементов
     * @return отсортированный массив
     * @since 11.11
     */

    public static void inEltmtnt(int[] a) {
        for(int number = 0; number < a.length; ++number) {
            Scanner in = new Scanner(System.in);
            a[number] = in.nextInt();
        }
    }
}
